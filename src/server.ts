import express from "express";
import * as dotenv from "dotenv";
import routes from "./routes";

dotenv.config();

const port = process.env.PORT;

const app: express.Application = express();

app.use(express.json());
app.use(routes);
app.listen(port, () => console.log(`App is listen on port ${port}`));
